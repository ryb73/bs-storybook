type t;
[@bs.module "@storybook/react"] external storiesOf : string => {. "__cache": Js.Dict.t(Node.node_module)} => t = "";

[@bs.send.pipe: t] external add : string => (unit => ReasonReact.reactElement) => t = "";
[@bs.send.pipe: t] external addDecorator : (([@bs] unit => _) => ReasonReact.reactElement) => t = "";
