type t;

let storiesOf: (string, {. "__cache": Js.Dict.t(Node.node_module)}) => t;

let add: (string, unit => ReasonReact.reactElement, t) => t;

let addDecorator: (([@bs] unit => 'a) => ReasonReact.reactElement, t) => t;
